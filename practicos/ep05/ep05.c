typedef unsigned char uchar_t;

/* ****************************************************************** **
 * PUNTO 1
 * ****************************************************************** */
/* Un lavarropas "inteligente" contiene, entre otros, el siguiente
 * registro de configuración:
 *     +-------+-------+-------+-------+-------+-------+-------+-------+
 * MSB |    PROGRAMA LAVADO    | MEDIO |  FRIO |       DIFERIDO        |
 *     +-------+-------+-------+-------+-------+-------+-------+-------+
 *     Los valores que pueden tener dichos campos son los de las
 *     siguientes tablas:
 *     PROGRAMA DE LAVADO  PROGRAMA      |    DIFERIDO   TIEMPO
 *        0    0    0      Rapido        |     0  0  0   Desactivado
 *        0    0    1      Delicado      |     0  0  1   2 horas
 *        0    1    0      Normal        |     0  1  0   4 horas
 *        0    1    1      Muy sucio     |     0  1  1   6 horas
 *        1    0    0      Lana          |     1  0  0   8 horas
 *        1    0    1      Reservado     |     1  0  1   10 horas
 *        1    1    0      Reservado     |     1  1  0   12 horas
 *        1    1    1      Reservado     |     1  1  1   14 horas
 * ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 1a
 * ****************************************************************** */
/* Defina el tipo programa_t con los elementos PRG_RAPIDO, PRG_DELICADO,
 * PRG_NORMAL, PRG_SUCIO, y PRG_LANA. Defina todas las máscaras y
 * corrimientos necesarios para utilizar el registro. */
/* ****************************************************************** */
#define MASK_PROGRAMA 0xE0
#define MASK_MEDIO 0x10
#define MASK_FRIO 0x08
#define MASK_DIFERIDO 0x07
#define SHIFT_PROGRAMA 5
#define SHIFT_MEDIO 4
#define SHIFT_FRIO 3
#define SHIFT_DIFERIDO 0

typedef enum {
    PRG_RAPIDO = 0,   /* 0x00 : 0x00 */
    PRG_DELICADO = 1, /* 0x01 : 0x20 */
    PRG_NORMAL = 2,   /* 0x02 : 0x40 */
    PRG_SUCIO = 3,    /* 0x03 : 0x60 */
    PRG_LANA = 4      /* 0x04 : 0x80 */
} programa_t;
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 1b
 * ****************************************************************** */
/* Implemente una función que reciba un registro y retorne, por la
 * interfaz, el programa cargado, y void por el nombre. */
void get_programa(uchar_t registro, programa_t * programa) {
    if (programa) {
        *programa = (registro & MASK_PROGRAMA) >> SHIFT_PROGRAMA;
    }
}
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 1c
 * ****************************************************************** */
/* Implemente una función que reciba un registo y retorne, por el
 * nombre, la cantidad de horas diferidas.
 *             short get_horas(unsigned char ?? reg); */
short get_horas(uchar_t registro) {
    return ((registro & MASK_DIFERIDO) >> SHIFT_DIFERIDO) * 2; /* << */
}
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 1d
 * ****************************************************************** */
/* Implemente una función que reciba un registro, el programa de lavado
 * y lo almacene en el registro.
 * Utilice el siguiente prototipo:
 *         ?? set_programa(unsigned char ??, programa_t ??); */
uchar_t set_programa(uchar_t registro, programa_t programa) {
    return (registro & ~MASK_PROGRAMA) |
           ((programa << SHIFT_PROGRAMA) & MASK_PROGRAMA);
}
void set_programa2(uchar_t * registro, programa_t programa) {
    if (registro) {
        *registro &= ~MASK_PROGRAMA;
        *registro |= ((programa << SHIFT_PROGRAMA) & MASK_PROGRAMA);
    }
}
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 1e
 * ****************************************************************** */
/* Dé ejemplos de uso de las funciones desarrolladas. */
void ejemplo_1e(void) {
    uchar_t ctrl;
    programa_t programa;
    short horas;

    get_programa(ctrl, &programa);
    horas = get_horas(ctrl);
    ctrl = set_programa(ctrl, programa);
    set_programa2(&ctrl, programa);
}
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 2
 * ****************************************************************** */
/* Implemente una función recursiva, potencia, que reciba un número
 * b ∈ R, y un número k ∈ Z, y retorne b^k.
 * Dé un ejemplo de uso de la función. */
double _potencia_recursiva(double b, unsigned int k) {
    double a;

    if (k == 1)
        return b;

    a = _potencia_recursiva(b, k/2);
    a *= a;

    return k & 0x1 ? a * b : a;
}

double potencia(double b, int k) {
    if (!k) {
        return !b ? 1 /* 0^0 -> boom */ : 1;
    }

    if (!b) {
        return k < 0 ? 1 /* 1/0 -> boom */ : 0;
    }

    if (b == 1) {
        return 1;
    }

    if (b == -1) {
        return k & 0x1 ? -1 : 1;
    }

    return (k < 0) ? _potencia_recursiva(1/b, -k) :
                     _potencia_recursiva(b, k);
}
/* ****************************************************************** */
