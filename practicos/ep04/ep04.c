#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STR     25
#define MAX_EQUIPOS 30

typedef enum {ST_OK, ST_ERROR, ST_ENOMEM, ST_EPTRNULO} status_t;

/* ****************************************************************** **
 * PUNTO 1a
 * ****************************************************************** */
/* Defina una estructura, llamada equipo, que contenga una variable para
 * almacenar un nombre, otra para almacenar la cantidad de partidos
 * ganados, otra para partidos empatados, otra para partidos perdidos,
 * otra para la cantidad de tantos a favor y en contra.  Y una segunda
 * estructura, torneo, que contenga el nombre del torneo, un arreglo
 * para los equipos, y la cantidad de equipos. */
struct equipo {
    char nombre[MAX_STR];
    int pg, pe, pp;
    int tf, tc;
};
typedef struct equipo equipo_s;

struct torneo {
    char nombre[MAX_STR];
    struct equipo equipos[MAX_EQUIPOS];
    size_t cantidad_equipos;
};
typedef struct torneo torneo_s;
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 1b
 * ****************************************************************** */
/* Implemente una función que inicialice un equipo con un nombre pasado
 * como argumento y todos los partidos en un valor razonable. Si
 * considera que la función podría ser vulnerable por algún motivo,
 * coméntelo. */
void inicializar_equipo(struct equipo * equipo, const char * nombre) {
    if (equipo && nombre) {
        strncpy(equipo->nombre, nombre, MAX_STR - 1);
        equipo->nombre[MAX_STR - 1] = '\0';
        equipo->pg = equipo->pe = equipo->pp = 0;
        equipo->tf = equipo->tc = 0;
    }
}
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 1c
 * ****************************************************************** */
/* Haga una función que inicialice un torneo con un arreglo de nombres
 * (equipo) recibido como argumento. */
void inicializar_torneo(struct torneo * torneo, const char * nombre,
                        const char * equipos[], size_t cantidad) {
    size_t i;

    if (torneo && nombre && equipos && cantidad <= MAX_EQUIPOS) {
        strncpy(torneo->nombre, nombre, MAX_STR - 1);
        torneo->nombre[MAX_STR - 1] = '\0';

        for (i = 0; i < cantidad; ++i) {
            inicializar_equipo(&torneo->equipos[i], equipos[i]);
        }
        torneo->cantidad_equipos = cantidad;
    }
}
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 1d
 * ****************************************************************** */
/* Escriba un main correspondiente a un programa que inicializa un
 * torneo con 3 equipos y, una vez creado el torneo, muestra los nombres
 * de los equipos y sus estadísticas, como se muestra:
        nombre de equipo,PG,PE,PP */
int main(void) {
    struct torneo torneo;
    size_t i;
    const char *nombres[] = {
        "Anna Ushenina",
        "Bobby Fisher",
        "Garry Kasparov"
    };

    inicializar_torneo(&torneo, "Grand Masters", nombres, 3);

    for (i = 0; i < torneo.cantidad_equipos; ++i) {
        printf("%s,%i,%i,%i\n", torneo.equipos[i].nombre,
                                torneo.equipos[i].pg,
                                torneo.equipos[i].pe,
                                torneo.equipos[i].pp);
    }

    return EXIT_SUCCESS;
}
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 2a
 * ****************************************************************** */
/* Implemente una función, join, que reciba un arreglo de cadenas, su
 * longitud, y un caracter delimitador y retorne por la interfaz una
 * cadena formada por todas las del arreglo, separadas por el caracter
 * delimitador. Por el nombre retorna el estado de la operación. Si
 * falla, se debe retornar NULL por la interfaz y un error por el
 * nombre. */
status_t join(const char *cadenas[], size_t l, char d, char ** yunta) {
    size_t i, largo;
    char * pc;

    if (!cadenas || !yunta || !l)
        return ST_ERROR;

    for (i = 0, largo = 0; i < l; ++i)
        largo += strlen(cadenas[i]);

    if (!(*yunta = (char *)malloc(sizeof(char) * (largo + l))))
        return ST_ENOMEM;

    strcpy(*yunta, cadenas[0]);
    for (i = 1, pc = *yunta; i < l; ++i) {
        pc += strlen(pc);
        *pc++ = d;
        strcpy(pc, cadenas[i]);
    }

    return ST_OK;
}
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 2b
 * ****************************************************************** */
/* Implemente una función análoga a la anterior pero retornando la
 * cadena generada por el nombre y un estado por la interfaz, si se
 * recibe para dicho estado un puntero distinto de NULL. */
char * join2(const char *cadenas[], size_t l, char d, status_t * st) {
    char * yunta;
    status_t estado;

    estado = join(cadenas, l, d, &yunta);
    if (st)
        *st = estado;

    return yunta;
}
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 2c
 * ****************************************************************** */
/* De ejemplos de uso las 2 funciones anteriores. */
void ejemplo_2c(void) {
    const char * cadenas[] = {"uno", "dos", "tres"};
    char * yunta;
    status_t st;
    st = join(cadenas, 3, ',', &yunta);
    yunta = join2(cadenas, 3, ',', &st);
}
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 2d
 * ****************************************************************** */
/* Implemente una función que cree una matriz dinámica de números,
 * inicializados aleatoriamente. La cantidad de filas y columnas se
 * recibe por la interfaz. Debe retornar la matriz creada por el nombre,
 * o NULL si falla. Debe retornar un status_t por la interfaz, indicando
 * el error u OK. */
void destruir_matriz(int *** m, size_t * filas) {
    size_t i;
    if (m && *m && filas) {
        for (i = 0; i < *filas; i++) {
            free((*m)[i]);
            (*m)[i] = NULL; /* si valido todo, hago esto */
        }
        *filas = 0;
        free(*m);
        *m = NULL;
    }
}

int ** crear_matriz(size_t nfilas, size_t ncolumnas, status_t * st) {
    status_t estado;
    size_t i, j;
    int ** m;

    if (!st)
        st = &estado; /* para que *st no me falle después */

    if (!nfilas || !ncolumnas) {
        *st = ST_EPTRNULO; /* por ejemplo acá */
        return NULL;
    }

    if ((m = (int **)malloc(sizeof(int *) * nfilas)) == NULL) {
        *st = ST_ENOMEM;
        return NULL;
    }

    for (i = 0; i < nfilas; ++i) {
        if ((m[i] = (int *)malloc(sizeof(int) * ncolumnas)) == NULL) {
            destruir_matriz(&m, &i);
            *st = ST_ENOMEM;
            return NULL;
        }
    }

    for (i = 0; i < nfilas; ++i)
        for (j = 0; j < ncolumnas; ++j)
            m[i][j] = rand() % 100;

    *st = ST_OK;
    return m;
}
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 2e
 * ****************************************************************** */
/* Haga una función análoga a la anterior, retornando el status_t por el
 * nombre y la matriz por la interfaz. */
status_t crear_matriz2(size_t filas, size_t columnas, int *** pmatriz) {
    status_t estado = ST_EPTRNULO;

    if (pmatriz) {
        *pmatriz = crear_matriz(filas, columnas, &estado);
    }

    return estado;
}
/* ****************************************************************** */

/* ****************************************************************** **
 * PUNTO 2f
 * ****************************************************************** */
/* De ejemplos de uso las 2 funciones anteriores. */
void ejemplo_2f(void) {
    int ** matriz;
    status_t estado;
    matriz = crear_matriz(3, 4, &estado);
    estado = crear_matriz2(3, 4, &matriz);
}
/* ****************************************************************** */
