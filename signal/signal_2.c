#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

volatile sig_atomic_t interrupted = false;

void imprimir(void) {
    puts("Hola interrupciones...");
}

void INThandler(int sig) {
	interrupted = true;
}

int main(void) {
	signal(SIGINT, INThandler);
    atexit(imprimir);

	while (!interrupted) {
		getchar();
	}

    return EXIT_SUCCESS;
}
