#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

void handler_SIGINT(int);
void handler_SIGQUIT(int);

volatile int sigquit_received = false;

int main (void) {
    signal(SIGINT, handler_SIGINT);    /* ^C se atrapa con handler_SIGINT */
    signal(SIGQUIT, handler_SIGQUIT);  /* ^\ se atrapa con handler_SIGQUIT */
    printf("%s\n", "Use ctrl-\\ para salir");

    do {
    } while (!sigquit_received) ;

    return EXIT_SUCCESS;
}

void handler_SIGINT (int s) {
    /* algunas versiones de UNIX reestablecen el handler de la señal. Se
     * vuelve a asignar para que, en la práctica, no cambie el handler
     * */
    signal(SIGINT, handler_SIGINT);

    /* no se debe interactuar con I/O en las interrupciones */
    fprintf(stderr, "%s\n", "CTRL+C no hace nada ahora");
}

void handler_SIGQUIT (int s) {
    /* no se debe interactuar con I/O en las interrupciones */
    fprintf(stderr, "%s\n", "CTRL+\\ fue presionado");
    sigquit_received = true;
}
